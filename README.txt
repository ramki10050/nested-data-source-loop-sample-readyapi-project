Nested data source sample
==============================

This sample project demonstrates how to use the nested data source loop.

Run steps
----------------------
1. Extract the project from the .zip archive.
2. In ReadyAPI, select "File > Import Project" from the main menu to open the project.
3. Open TestCase 1 and take a look at the data source loops. Examine the REST Request and Data Source 
    test steps. 
4. The project has a couple of data sources with loops. Each data source contains two values. 
5. Run the test case.
6. In the test case editor, select the Transaction Log tab.
    
Expected response
----------------------
You will see about 4 transactions (2 transactions per loop) of REST Request. Open every REST Request 
and select the Raw tab. In the first line you will see the query parameters. For each transaction, parameter 
values will be different as ReadyAPI looped through data sources.